Pagina che raccoglie le presentazioni che ho realizzato nel corso degli anni. Sono per lo più realizzate con *Beamer*, pacchetto di *LaTeX* costruito allo scopo. Per alcune di esse propongo oltre al *file* .pdf, anche il sorgente .tex.
Inoltre in questa nuova versione ampliata rispetto all'[originale su GitHub](https://github.com/ulaulaman/presentazioni) propongo anche alcuni *ebook* (permettetemi di chiamarli in questo modo).

# E-book

* [*Rappresentazioni proiettive: Applicazioni nella teoria quantistica*](ebook/rappresentazioni_proiettive.pdf) (tesi di dottorato)
* [*Galileo Galilei. Riflessioni su un fisico*](ebook/galileo_galilei.pdf) con **Marco Fulvio Barozzi**
* [*Night in Cosmo Brain*](ebook/night_cosmobrain.pdf)
* [*Notizie pi greche*](ebook/piday.pdf)
* [*AIDS*](ebook/Madonna-AIDS.pdf) - fumetto distribuito al concerto del 13 luglio 1987 di **Madonna** al Madison Square Garden

# Presentazioni

## La fisica con i supereroi

* *Piccolo è meglio*: Ant-Man ([pdf](supereroi/ant-man.pdf))
* *In fondo al mar*: Aquaman ([pdf](supereroi/aquaman.pdf))
* *Conviene farsi salvare da Batman*: Batman vs Spider-Man ([pdf](supereroi/batman_spiderman.pdf))
* *Lo scudo di Capitan America* ([pdf](supereroi/cap_america.pdf))
* *Meraviglie cosmiche*: Capitan Marvel ([pdf](supereroi/cap_marvel.pdf))
* *Quelli che cavalcano il fulmine*: Flash ([pdf](supereroi/flash.pdf))
* *Supereroi su Marte*: Justice Society of America ([pdf](supereroi/jsa_marte.pdf))
* *Su, su e via*: Superman ([pdf](supereroi/superman.pdf))

## Presentazioni su Kerbal Space Program

* *Da marte a Kerbin passando per Brera*, con Stefano Sandrelli ([pdf](kerbal/intro_kerbal.pdf))
* *Kerbal for teachers*, con Agatino Rifatto per il *Maker Faire* di Roma del 2019 ([pdf](kerbal/kerbal_roma.pdf))

## Liceo Cavalleri di Parabiago

Presentazioni realizzate per una serie di incontri con gli insegnanti al Liceo *Cavalleri* di Parabiago (Milano)

* *L'universo ottico* ([pdf](parabiago/pdf/universo_ottico.pdf) | [tex](parabiago/universo_ottico.tex))
* *Astronomia planetaria: transiti ed esopianeti in classe* ([pdf](parabiago/pdf/transito.pdf) | [tex](parabiago/transito.tex))
* *Il meraviglioso mondo quantistico* ([pdf](parabiago/pdf/mondo_quantistico.pdf) | [tex](parabiago/mondo_quantistico.tex))
* *Vincere un Nobel per la fisica con i disegni* ([pdf](parabiago/pdf/feynman.pdf) | [tex](parabiago/feynman.tex))
* *Una storia di paradossi, disuguaglianze e baffi* ([pdf](parabiago/pdf/paradossi_disuguaglianze.pdf) | [tex](parabiago/paradossi_disuguaglianze.tex))

*Come scrivere un articolo divulgativo sulla scienza*: intervento in una classe quarta
* Presentazione ([pdf](parabiago/pdf/parabiago_IFS.pdf) | [tex](parabiago/parabiago_IFS.tex))
* Testo intervento ([pdf](parabiago/pdf/parabiago_IFS-articolo.pdf) | [tex](parabiago/parabiago_IFS-articolo.tex))

## Conferenze a congressi

* *Congresso SAIt 2017* ([pdf](conferenze/pdf/SAIt2017-filippelli_barbalini.pdf) | [tex](conferenze/SAIt2017.tex))
* *Time dependent quantum generators for the Galilei group*, Praga 2007 ([pdf](conferenze/pdf/extensionGF.pdf))
* *ComunicareFisica 2012* ([pdf](conferenze/pdf/comunicarefisica2012.pdf) | [tex](conferenze/comunicarefisica2012.tex))
* *Justice in Space* a Lucca Comics&Science 2023 ([pdf](supereroi/justice_in_space-lucca2023.pdf))

## Presentazioni varie

* *Scrivere in HTML*, presentazione presso il Liceo Scientifico Scorza di Cosenza ([pdf](conferenze/pdf/html.pdf))
* *Wikipedia: Template*, presentazione presso l'Università della Calabria ([pdf](conferenze/pdf/wikiT.pdf))
